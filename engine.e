note
	description: "The engine to show the informations"
	author: "Louis Marchand"
	date: "Thu, 11 May 2023 23:29:11 +0000"
	revision: "01"

class
	ENGINE

inherit
	GAME_LIBRARY_SHARED

create
	make

feature {NONE} -- Initialisation

	make(a_images:MNIST_IMAGES)
			-- Initialisation of `Current'.
		require
			No_Error: not a_images.has_error
			At_Least_One_Image: a_images.samples_count > 0
		local
			l_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			index := 1
			has_error := False
			images := a_images
			create l_builder
			l_builder.set_dimension (600, 600)
			window := l_builder.generate_window
			previous_button := load_texture_from_file("previous.png")
			next_button := load_texture_from_file("next.png")
			open_font
			if font.is_open then
				load_image(index)
			else
				has_error := True
				create image.make (window.renderer, default_pixel_format, 1, 1)
				create label.make (window.renderer, default_pixel_format, 1, 1)
			end
		end

	open_font
			-- Open the `font` file
		do
			create font.make ("font.ttf", 20)
			if font.is_openable then
				font.open
			end
		end

feature -- Access

	has_error:BOOLEAN
			-- An error occured.

	run
			-- Execute the engine
		require
			No_Error: not has_error
		do
			game_library.iteration_actions.extend (agent on_iteration)
			game_library.quit_signal_actions.extend (agent on_quit)
			window.mouse_button_released_actions.extend (agent on_mouse_pressed)
			game_library.launch
		end

	images:MNIST_IMAGES
			-- The MNIST images manager

	index:INTEGER
			-- The index of the MNIST image to show

	font:TEXT_FONT
			-- The font used to show the label


	load_image(a_index:INTEGER)
			-- Load MNIST image indexed as `a_index' in `image`
		require
			Valid_Index: a_index > 0 and a_index <= images.samples_count
			Font_Valid: not font.has_error
		local
			l_image_target:GAME_TEXTURE_TARGET
			l_data:ARRAY2[NATURAL_8]
			l_label:INTEGER
			l_label_surface:TEXT_SURFACE_BLENDED
			l_color:GAME_COLOR
		do
			l_data := images.image_data (a_index)
			l_label := images.image_label (a_index)
			create l_image_target.make (window.renderer, default_pixel_format, l_data.width, l_data.height)
			window.renderer.set_target (l_image_target)
			across 1 |..| l_data.width as la_column loop
				across 1 |..| l_data.height as la_line loop
					create l_color.make_rgb ({NATURAL_8}255 - l_data[la_column.item, la_line.item], {NATURAL_8}255 - l_data[la_column.item, la_line.item], {NATURAL_8}255 - l_data[la_column.item, la_line.item])
					window.renderer.set_drawing_color (l_color)
					window.renderer.draw_point (la_column.item - 1, la_line.item - 1)
				end
			end
			window.renderer.set_original_target
			image := l_image_target
			create l_label_surface.make (l_label.out, font, create {GAME_COLOR}.make_rgb (0, 0, 0))
			create label.make_from_surface (window.renderer, l_label_surface)
		end


feature {NONE} -- Implementation


	window:GAME_WINDOW_RENDERED
			-- The window used to draw the informations

	previous_button:GAME_TEXTURE
			-- The button used to go to previous image

	next_button:GAME_TEXTURE
			-- The button used to go to next image

	image:GAME_TEXTURE
			-- The MNIST image to show

	label:GAME_TEXTURE
			-- The label of the NMIST `image`


	on_iteration(a_timestamp:NATURAL)
			-- Launched at each loop iteration
		do
			window.renderer.set_drawing_color (create {GAME_COLOR}.make_rgb (128, 128, 128))
			window.renderer.clear
			window.renderer.draw_sub_texture_with_scale (image, 0, 0, image.width, image.height, 100, 50, 400, 400)
			window.renderer.draw_texture (label, (window.width // 2) - (label.width // 2), 500)
			window.renderer.draw_texture (previous_button, 10, (window.height // 2) - (previous_button.height // 2))
			window.renderer.draw_texture (next_button, window.width - next_button.width - 10, (window.height // 2) - (next_button.height // 2))
			window.renderer.present
		end

	on_mouse_pressed(a_timestamp:NATURAL_32; a_mouse_state:GAME_MOUSE_BUTTON_RELEASE_EVENT; a_nb_clicks:NATURAL_8)
			-- Launched when the user clicked on the `window`
		do
			if
				index > 1 and
				a_mouse_state.x > 10 and a_mouse_state.x < previous_button.width + 10 and
				a_mouse_state.y > (window.height // 2) - (previous_button.height // 2) and a_mouse_state.y < (window.height // 2) + (previous_button.height // 2)
			then
				index := index - 1
				load_image(index)
			end
			if
				index < images.samples_count and
				a_mouse_state.x > window.width - next_button.width - 10 and a_mouse_state.x < window.width - 10 and
				a_mouse_state.y > (window.height // 2) - (next_button.height // 2) and a_mouse_state.y < (window.height // 2) + (previous_button.height // 2)
			then
				index := index + 1
				load_image(index)
			end
		end

	on_quit(a_timestamp:NATURAL)
			-- Launched when the user closed the `window`
		do
			game_library.stop
		end

	load_texture_from_file(a_file:READABLE_STRING_GENERAL):GAME_TEXTURE
			-- Load a {GAME_TEXTURE} from an image `a_file'
		local
			l_image:IMG_IMAGE_FILE
		do
			create l_image.make (a_file)
			if l_image.is_openable then
				l_image.open
				if l_image.is_open then
					create Result.make_from_image (window.renderer, l_image)
				else
					has_error := True
					create Result.make (window.renderer, default_pixel_format, 1, 1)
				end
			else
				has_error := True
				create Result.make (window.renderer, default_pixel_format, 1, 1)
			end
		end

	default_pixel_format:GAME_PIXEL_FORMAT
			-- A default {GAME_PIXEL_FORMAT} used when an error occuded.
		local
			l_pixel:GAME_PIXEL_FORMAT
		do
			create Result
			Result.set_rgba8888

		end
end
