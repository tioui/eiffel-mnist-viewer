note
	description: "A MNIST image viewer"
	author: "Louis Marchand"
	date: "Thu, 11 May 2023 23:29:11 +0000"
	revision: "01"

class
	APPLICATION

inherit
	ARGUMENTS
	INTEGER_READER
	GAME_LIBRARY_SHARED
	TEXT_LIBRARY_SHARED
	IMG_LIBRARY_SHARED

create
	make

feature {NONE} -- Initialization

	make
			-- Running the application
		local
			l_engine:ENGINE
		do
			game_library.enable_video
			text_library.enable_text
			image_file_library.enable_png
			prepare_images
			if attached images as la_images and then la_images.samples_count > 0 then
				create l_engine.make(la_images)
				if not l_engine.has_error then
					l_engine.run
				else
					io.error.put_string_32 ("An error ocured. Closing.%N")
				end
			else
				io.error.put_string ("An error occured. Closing.%N")
			end
		end


feature {NONE} -- Implementation

	images:detachable MNIST_IMAGES
			-- An MNIT image manager

	prepare_images
			-- Prepare `images`
		local
			l_images: MNIST_IMAGES
		do
			if argument_count < 2 then
				io.error.put_string ("Usage: " + argument (0) + " <Data File> <Label File>%N")
			else
				create l_images.make (argument (1), argument (2))
				if l_images.has_error then
					io.error.put_string_32 (l_images.error)
				else
					images := l_images
				end
			end
		end




end
