note
	description: "Used to manage MNIST images data dans label files"
	author: "Louis Marchand"
	date: "Thu, 11 May 2023 23:29:11 +0000"
	revision: "01"

class
	MNIST_IMAGES

inherit
	INTEGER_READER

create
	make

feature {NONE} -- Initialisation

	make(a_mnist_data_file, a_mnist_label_file:READABLE_STRING_GENERAL)
			-- Initialisation of `Current' using `a_mnist_data_file' as data file and `a_mnist_label_file'
			-- as index file.
		do
			error := ""
			create data_file.make_with_name (a_mnist_data_file)
			create label_file.make_with_name (a_mnist_label_file)
			prepare_data(data_file)
			prepare_label(label_file)
		end

	prepare_data(a_data_file:RAW_FILE)
			-- Prepare the opening of `a_data_file'
		do
			if a_data_file.exists and a_data_file.is_access_readable then
				a_data_file.open_read
			else
				error := "File " + a_data_file.path.name +  " cannot be openned%N"
				has_error := True
			end
		end

	prepare_label(a_label_file:RAW_FILE)
			-- Prepare the opening of `a_label_file'
		local
			l_magic_word:INTEGER_32
		do
			if a_label_file.exists and a_label_file.is_access_readable then
				a_label_file.open_read
				read_integer_32(a_label_file)
				l_magic_word := last_integer_32
				if l_magic_word = Label_File_Magic_Word then
					read_integer_32(a_label_file)
					samples_count := last_integer_32
					read_integer_32(a_label_file)
					column_count := last_integer_32
				else
					error := "File " + a_label_file.path.name +  " is not a valid label file%N"
					has_error := True
				end
			else
				error := "File " + a_label_file.path.name +  " cannot be openned%N"
				has_error := True
			end
		end

feature -- Access

	samples_count:INTEGER_32
			-- The number of samples in the files

	column_count:INTEGER_32
			-- The number of column in the label file

	has_error:BOOLEAN
			-- An error occured while loading the files

	error:STRING_32
			-- The error that has occured when `has_error` is `True'

	image_data(a_index:INTEGER):ARRAY2[NATURAL_8]
			-- Load the `a_index'-th image data and return the result as a 2d array of bytes.
		do
			data_file.go (((a_index - 1) * (Image_Width * Image_Height)) + Data_Header_Count)
			create Result.make_filled (0, Image_Width, image_Height)
			across 1 |..| Image_Height as la_line loop
				across 1 |..| Image_Width as la_column loop
					data_file.read_natural_8
					Result[la_column.item, la_line.item] := data_file.last_natural_8
				end
			end
		end

	image_label(a_index:INTEGER):INTEGER
			-- Load the `a_index'-th image label.
		do
			label_file.go ((((a_index - 1) * column_count) + Label_Header_Count) * 4)
			read_integer_32 (label_file)
			Result := last_integer_32
		end


feature {NONE} -- Implementation

	data_file:RAW_FILE
			-- The data file

	label_file:RAW_FILE
			-- The label file

feature -- constants

	Label_File_Magic_Word:INTEGER_32 = 3074
			-- The magic word of the label file

	Image_Width:INTEGER = 28
			-- The horizontal dimension of an image in the data file
	Image_Height:INTEGER = 28
			-- The vertical dimension of an image in the data file

	Label_Header_Count:INTEGER = 3
			-- The number of information in the label file's header

	Data_Header_Count:INTEGER = 16
			-- The number of information in the data file's header

end
