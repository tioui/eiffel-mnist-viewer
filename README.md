MNIST Data viewer
=================


This program is used to View image data and label from the MNIST files. You can find the MNIST files here: https://github.com/facebookresearch/qmnist .

Prerequiste
-----------

To compile the Viewer, you will need:
* EiffelStudio: https://account.eiffel.com/downloads
* EiffelGame2: https://www.eiffelgame2.org/index.php/download/

Usage
-----

To launch the program, you need to give as command parameters the path to the data file and to the label file. Like this:

```bash
./mnist_reader qmnist-train-images-idx3-ubyte qmnist-train-labels-idx2-int
```

Note that you can also put the parameters in the run profile of EiffelStudio.

To work correctly, the png files (previous.png and next.png) and the font.ttf file must be in the same directory as the executable. Note also that, on Windows, you also need EiffelGame2 DLLs in the directory. Also, the execution on Windows is like this:

```bash
mnist_reader.exe qmnist-train-images-idx3-ubyte qmnist-train-labels-idx2-int
```

License
-------

Copyright (c) 2023 Louis M

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

