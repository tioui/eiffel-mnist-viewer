note
	description: "Used to read an INTEGER un big endian order"
	author: "Louis Marchand"
	date: "Thu, 11 May 2023 23:29:11 +0000"
	revision: "01"

class
	INTEGER_READER

feature {NONE} -- Implementation

	last_integer_32:INTEGER_32
			-- The last {INTEGER} readed by `read_integer_32'

	read_integer_32(a_file:RAW_FILE)
			-- Read an integer from `a_file' in big endian order. Put the result in `last_integer_32'
		local
			l_byte:NATURAL_8
			l_natural:NATURAL_32
		do
			l_natural := 0
			across 1 |..| 4 as la_index loop
				a_file.read_natural_8
				l_byte := a_file.last_natural_8
				l_natural := l_natural.bit_or (l_byte.to_natural_32.bit_shift_left ((4 - la_index.item) * 8))
			end
			last_integer_32 := l_natural.as_integer_32
		end

end
